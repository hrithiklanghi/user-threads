#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>
#include <unistd.h>
#include "one2one.h"

int handler_val = 0;
void *test(void *arg){
    int *temp = (int*)arg;
    printf("Test result: %d\n", *temp);
    int rc = sleep(20);
    if(rc != 0 || errno == EINTR){
        printf("Thread recieved a signal.\n");
        return NULL;
    }
    return (void*)temp;
}

void sig_handler(int signo){
    printf("signal handler running.\n");
    handler_val = 0;
}

void *kill_fun(void *arg){
    while(handler_val);
    return (void *)128;
}

void *join_test(int *arr){
    for(i=0;i<10000;i++){
        arr[i]=0;
    }
    for(i=0;i<10000;i++){
        arr[i]=arr[i]+1;
    }
}
int main(){
    mythread_init();
    mythread_t new_thread;
    //Test for creating a thread in one-one model
    int i=20;
    int *pass = &i;
    int x = mythread_create(&new_thread, NULL, test, (void*)pass);
    
    if(x == 0){
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    //Test for creating a thread to have null function
    x = mythread_create(&new_thread, NULL, NULL, (void*)temp);
    
    if(x == 22){
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    //Test for creating a thread with null pointer
    x = mythread_create(NULL, NULL, fun, (void*)temp);
    
    if(x == 22){
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    //Test to pass mythread_kill() signal to a thread
    signal(SIGUSR1, sighandler);
    handler_val=1;
    void *ret;
    x = mythread_create(&new_thread, NULL, killfun, NULL);
    mythread_kill(new_thread, SIGUSR1);
    mythread_join(new_thread, &ret);
    sleep(10);

    if(handler_val == 0){
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    mythread_t  t1, t2;
    int arr1[10000];
    int arr2[10000];

    (void) mythread_create(&t1, NULL, join_fun, &arr1);
    (void) mythread_create(&t1, NULL, join_fun, &arr2);
    int y = mythread_join(t1, NULL);
    int z = mythread_join(t2, NULL);
    //Test for joining 2 threads
    if(y == 0 && z == 0){
        printf("-----Test passed-----\n");
    }
    else{
        printf("-----Test failed %d-----\n", y);
    }

    y = mythread_join(th1, NULL);
    //Test for joining the thread that is already joined
        if(y == 22){
            printf("-----Test passed-----\n");
        }
        else{
            printf("-----Test Failed-----\n");
        }
     return 0;
}