#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sched.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <linux/futex.h>
#include <sys/time.h>
#include <sys/types.h>
#include "one2one.h"

#define _GNU_SOURCE

queue *threads_queue;

mythread* curr_thread(void){
    long address;
    syscall(SYS_arch_prctl, ARCH_GET_FS, &address);
    return (mythread*)address;
}

int thread_function(void *thread){
    mythread *t = (mythread*)thread;
    t -> return_value = t -> thread_start(t -> args);
    t -> state = THREAD_EXITED;
    return 0;
}

int mythread_init(void){
    mythread *thread = (mythread*)calloc(1, sizeof(mythread));
    thread -> args = NULL;
    thread -> stack_size = 0;
    thread -> tid = syscall(SYS_gettid);
    thread -> futex = 0;
    thread -> thread_start = NULL;
    thread -> return_value = NULL;
    thread -> stack = NULL;
    
    return 0; 
}


int mythread_create(mythread_t *thread_id, void *(*thread_start)(void *), void *args){
    
    if(!thread || !thread_start){
        errno = EINVAL;
        return errno;
    }
          
    mythread *new_thread = (mythread*)calloc(1, sizeof(mythread));
    if(!new_thread){
        errno = ENOMEM;
        return errno;
    }

    new_thread -> thread_start = thread_start; 
    new_thread -> stack_size = STACK_SIZE;
    new_thread -> state = THREAD_RUNNING;
    new_thread -> args = args;

    size_t pg_size = sysconf(_SC_PAGESIZE); 
    void *alloc_stk = mmap(NULL, new_thread->stack_size + pg_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    new_thread -> stack = alloc_stk + pg_size;

    new_thread -> tid = clone( &thread_function, new_thread -> stack + new_thread -> stack_size, CLONE_VM | CLONE_SIGHAND 
    | CLONE_FS | CLONE_FILES | CLONE_THREAD | CLONE_SYSVSEM | CLONE_SETTLS | CLONE_CHILD_CLEARTID | CLONE_PARENT_SETTID, new_thread, new_thread -> futex, new_thread, new_thread -> futex);   

    if(new_thread -> tid == EINVAL || new_thread -> tid == ENOMEM){
        printf("Unable to form stack!");
    }

    *thread = new_thread -> tid;

    if(new_thread -> tid < 0){
        printf("Error in clone\n");
        errno = EINVAL;
        return errno;
    } 
    
    return 0;
}

int mythread_join(mythread_t thread,void **return_value){
    mythread *t = (mythread*)malloc(sizeof(mythread));
    t = get_from_list(threads_queue, thread);

    if(isinlist(threads_queue, &t) <= 0){
        errno = ESRCH;
        return errno;
    }
    if(t->state == THREAD_JOINED){
        errno = EINVAL;
        return errno;
    }

    if(t->futex){
        t->state = THREAD_WAITING;
        syscall(SYS_futex, thread_join -> futex, FUTEX_WAIT, thread_join -> tid, NULL, NULL, 0);
    }

    t->state = THREAD_JOINED;
    if(return_value){
        *return_value = t->return_value;
    }
    return 0;
}

void mythread_exit(){
    mythread *t=curr_thread();
    if(t == NULL){
        printf("Null Thread.\n");
    }
    t->return_value = ret;
    t->state = THREAD_EXITED;
    syscall(SYS_exit, 0);
}

int mythread_kill(mythread_t this_thread, int sig){
    mythread *t=(mythread*)malloc(sizeof(mythread));
    t=fetch(threads_queue, this_thread);
    if(t->state == THREAD_EXITED || t->state == THREAD_JOINED){
        errno == EINVAL;
        return errno;
    }
    int kid = getpid();
    syscall(SYS_tgkill, kid, thread_kill -> tid, sig);
    return 0;
}

void append(mythread thread, queue q){
    node *add_new=(node*)malloc(sizeof(node));
    add_new->new_thread=thread;
    add_new->front=NULL;

    if(isEmpty(q)){
        add_new->back=NULL;
    }
    else{
        add_new->back=q->tail;
        q->tail->front=add_new;
        q->tail=add_new;
    }
}

void pop(q){
    if(isEmpty){
        return NULL;
    }
    
    mythread *t = (mythread*)malloc(sizeof(mythread));
    node *n = (node*)malloc(sizeof(node*));
    n = l->head;
    ret = temp->list_thread;
    l->head = n->next;
    n->next = NULL;
    n->prev = NULL;

    if(l -> head == NULL){
        l -> tail = NULL;
    }
    else{
        l -> head -> prev = NULL;
    }
    
    size_t pg_size = sysconf(_SC_PAGESIZE);
    munmap(t->stack - pg_size, t->stacksize + pg_size);
}

mythread *fetch(queue *q, mythread t){
    node *n = (node*)malloc(sizeof(node));
    n = q -> head;
    while(n -> next != NULL){
        if((n) -> list_thread -> tid == t)){
            return n->list_thread;
            }
        n = n -> next;
    }
    return NULL;
}

int count_thread(queue *q){
    node *n = (node*)malloc(sizeof(node));
    n = q -> head;
    int count = 0;
    while(n != NULL){
        count++;
        n = n -> next;
    }
    return count;
}

int isEmpty(list *l){
    return (!l->head && !l->tail);
}

int at_index(queue *q, mythread_t *t){
    node *n = (node*)malloc(sizeof(node));
    n = q -> head;
    int count = 0;
    while( && n -> next != NULL){
        if((n) -> list_thread -> tid == *t){
            return (pos + 1);
        }
        count++;
        n = n -> next;
    }
        return -1;    
}

void freeup(){
    int n = count_thread(threads_queue);

    while(n > 0){
        pop(threads_queue)
        n--;
    }
    free(threads_queue);
}
