#ifndef _THREAD_H
#define _THREAD_H

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 1024*64
//64KB

typedef pid_t mythread_t;

enum {
    THREAD_RUNNING,
    THREAD_EXITED,
    THREAD_JOINED,
    THREAD_WAITING,
    THREAD_READY,
    THREAD_BLOCKED
};

typedef struct mythread{
    mythread_t tid;             
    void *stack;                 
    size_t stack_size;          
    void *return_value;         
    struct mythread *next;      
    void *(*thread_start)(void *);          
    int state;                  
    void *args;                 
}mythread;

typedef struct node {       
    mythread *new_thread;
    struct node *back;
    struct node *front;
}node;

typedef struct queue {
    struct node *head;
    struct node *tail;
}queue;

//functions to operate queue
void append(mythread *thread, queue *new_queue);
void pop(queue *new_queue);
mythread *fetch(queue *q, mythread t);
int at_index(queue *q, mythread *t);
int count_thread(queue *new_queue);
int isEmpty(queue *new_queue);


int mythread_init(void);
int mythread_create(mythread_t *thread, void *(*thread_start)(void *), void *args);
int mythread_join(mythread_t thread,void **return_value);
void mythread_exit();
int mythread_kill();
void freeup();

#endif
